//
//  Logger.swift
//  BeeLocation
//
//  Created by Wang Luo on 10/5/21.
//

import Foundation
import SeaLogger

public struct Logger {
    public static var logger: SeaLogger.Logger.ContextLogger = SeaLogger.Logger.general

    private static let tag = "[BeeLocation] "

    static func info(_ text: String = "", file: StaticString = #file,
                     function: StaticString = #function, line: UInt = #line) {
        logger.info(tag + text, file: file, function: function, line: line)
    }

    static func warning(_ text: String = "", file: StaticString = #file,
                        function: StaticString = #function, line: UInt = #line) {
        logger.warning(tag + text, file: file, function: function, line: line)
    }

    static func error(_ text: String = "", file: StaticString = #file,
                      function: StaticString = #function, line: UInt = #line) {
        logger.error(tag + text, file: file, function: function, line: line)
    }

    static func debug(_ text: String = "", file: StaticString = #file,
                      function: StaticString = #function, line: UInt = #line) {
        logger.debug(tag + text, file: file, function: function, line: line)
    }

    static func verbose(_ text: String = "", file: StaticString = #file,
                        function: StaticString = #function, line: UInt = #line) {
        logger.verbose(tag + text, file: file, function: function, line: line)
    }
}
