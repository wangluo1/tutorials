//
//  ClientStream.swift
//  BeeLocation
//
//  Created by Wang Luo on 10/5/21.
//  Copyright © 2021 Wang Luo. All rights reserved.
//

import Foundation 
import ModuleBridge
import BeeLocationProtocol

class ClientStream: SessionModuleProtocol {
    
    private var session: ClientSession?

    required init() {}   
}

extension ClientStream {
    static var identifier: ModuleIdentifier {
        return .beelocation
    }

    func willBeginSession(bridge: ModuleBridge, sessionIdentifier: SessionIdentifier) {
        Logger.info()
    }

    func didBeginSession(bridge: ModuleBridge, sessionIdentifier: SessionIdentifier) {
        Logger.info()
    }

    func willEndSession(bridge: ModuleBridge, sessionIdentifier: SessionIdentifier) {
        Logger.info()
    }
    
    func didEndSession(bridge: ModuleBridge, sessionIdentifier: SessionIdentifier) {
        Logger.info()
        session = nil
    }
    
    func didAuthenticateSession(bridge: ModuleBridge, sessionIdentifier: SessionIdentifier) {
        Logger.info()
    }
    
    func didDeauthenticateSession(bridge: ModuleBridge, sessionIdentifier: SessionIdentifier) {
        Logger.info()
    }
    
    func moduleDidLoad(bridge: ModuleBridge) {
        Logger.info()
    }
}