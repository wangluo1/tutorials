//
//  ModuleIdentifier+BeeLocation.swift
//  BeeLocation
//
//  Created by Wang Luo on 10/5/21.
//  Copyright © 2021 Wang Luo. All rights reserved.
//

import Foundation 
import ModuleBridge

extension ModuleIdentifier {
    public static let beelocation = ModuleIdentifier(rawValue: "com.seagroup.seatalk.module.beelocation")
}

extension ModuleBridge {
    public func beelocationModule() throws -> BeeLocationProtocol {
        guard let module = try resolve(.beelocation) as? BeeLocationProtocol else {
            throw ModuleBridgeError.moduleNotFound(.beelocation)
        }
        
        return module
    }
}