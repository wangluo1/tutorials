//  GoogleLoginHandler.swift
//  Demo
//
//  Created by Kunzhen Wang on 19/11/20.
//

import Foundation
import SeaTalkAuth
import GoogleSignIn
import SeaReactive
import SeaLogger

class GoogleLoginHandler: LoginOptionHandlable {
    let loginProvider: AuthInfo.Provider = .google
    let loginButtonIcon: UIImage = UIImage(named: "gmail_icon")!
    
    private weak var authSession: AuthSession?
    
    init(authSession: AuthSession) {
        self.authSession = authSession
        
        GIDSignIn.sharedInstance()?.clientID = "340437035612-m0uo3d0vcphop29v2v2qcmmuqib09tp6.apps.googleusercontent.com"
    }
    
    func beginLoginFlow(with emailAddress: String?, from viewController: AuthViewController) {
        var flow: Flow? = Flow(emailAddress: emailAddress, authSession: authSession, viewController: viewController)
        
        viewController.hudShowLoading()
        flow?.start().observeOnMain(forLifetimeOf: self).startWithResult { [weak viewController] result in
            defer { flow = nil }
            
            viewController?.hudHide()
            if case let .failure(error) = result {
                Logger.error("\(error)")
                GIDSignIn.sharedInstance().signOut()
                viewController?.handleError(error, canShowRestricedOnView: true)
            }
        }
    }
    
    func logOut() {
        GIDSignIn.sharedInstance()?.signOut()
    }
}

extension GoogleLoginHandler {
    fileprivate class Flow: NSObject {
        private let emailAddress: String?
        private weak var viewController: AuthViewController?
        
        private let authSession: AuthSession
        private var authHandler: OAuthHandlerProtocol { return authSession.oauthHandler }
        private var imageClient: ImageClientProtocol { return authSession.imageClient }
        private var uiNavigator: AuthUINavigatorProtocol { return authSession.uiNavigator }

        
        private let (executionSignal, executionObserver) = Signal<NoValue, Error>.pipe()
        private var isStarted: Bool = false
        private var executionResult: Result<NoValue, Error>? = nil
        
        init(emailAddress: String?, authSession: AuthSession?, viewController: AuthViewController) {
            self.emailAddress = emailAddress
            self.viewController = viewController
            self.authSession = authSession!
            super.init()
            executionSignal.take(first: 1)
                .observeOnMain(forLifetimeOf: self)
                .observeResult { [weak self] in self?.executionResult = $0 }
        }
        
        deinit {
            executionObserver.sendCompleted()
        }
        
        func start() -> SignalProducer<NoValue, Error> {
            return SignalProducer { [weak self] () -> SignalProducer<NoValue, Error> in
                guard let self = self else { return .weakError }
                if let result = self.executionResult { return .init(result: result) }
                if !self.isStarted {
                    self.isStarted = true
                    GIDSignIn.sharedInstance().delegate = self
                    GIDSignIn.sharedInstance()?.presentingViewController = self.viewController
                    GIDSignIn.sharedInstance().loginHint = self.emailAddress
                    GIDSignIn.sharedInstance().signIn()
                }
                return self.executionSignal.producer
            }
        }
        
        private func completeLoginFlow(withEmailAddress emailAddress: String, idToken: String, name: String, avatarURL: URL?) {
            verifyOAuth(withIDToken: idToken).startWithResult { [weak self] result in
                guard let self = self else { return }
                switch result {
                case .success(.noAction):
                    self.executionObserver.complete(with: .none)
                case .failure(let error):
                    self.executionObserver.send(error: error)
                case .success(.updateProfile(let info)):
                    self.imageClient.updateFileServerToken(info.fileServerToken, withUserID: info.fileServerUserID)
                    
                    self.uploadAvatarIfNeeded(fromURL: avatarURL).startWithValues { [weak self] avatar in
                        guard let self = self else { return }
                        guard let viewController = self.viewController else { return self.executionObserver.sendWeakError() }
                        
                        let prefilledProfileState = AuthPrefilledProfileState.present(AuthPrefilledProfile(
                            name: name,
                            avatarImage: avatar.map { .adhoc($0.image) } ?? .unset,
                            avatarFileName: avatar?.fileName
                        ), alsoHasHRISProfile: info.isSeaEmployee)
                        self.uiNavigator.navigateToProfileUpdate(withOAuthHandle: info.handle,
                                                                 provider: .google,
                                                                 registrationToken: info.registrationToken,
                                                                 prefilledProfileState: prefilledProfileState,
                                                                 from: viewController)

                        self.executionObserver.complete(with: .none)
                    }
                }
            }
        }
        
        private func verifyOAuth(withIDToken idToken: String) -> SignalProducer<OAuthVerificationResult, Error> {
            return SignalProducer { [weak self] () -> SignalProducer<OAuthVerificationResult, Error> in
                guard let self = self else { return .weakError }
                return self.authHandler.authenticateUsingOAuth(provider: .google, withToken: idToken)
            }
        }
        
        private func uploadAvatarIfNeeded(fromURL avatarURL: URL?) -> SignalProducer<(image: UIImage, fileName: String)?, Never> {
            // No upload required if no avatar is present
            guard let avatarURL = avatarURL else { return .value(nil) }
            
            return imageClient.downloadImage(fromURL: avatarURL)
                .flatMapLatest { [weak self] image -> SignalProducer<(image: UIImage, fileName: String), Error> in
                    guard let self = self else { return .weakError }
                    return self.imageClient.uploadImageAfterCompressing(image).map { fileName in (image, fileName) }
            }
            .map { $0 as (image: UIImage, fileName: String)? }
            .flatMapError { error -> SignalProducer<(image: UIImage, fileName: String)?, Never> in
                Logger.error("\(error)")
                return .value(nil)
            }
        }
    }
}

// MARK: - GIDSignInDelegate
extension GoogleLoginHandler.Flow: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        Logger.verbose()
        
        if let error = error {
            if (error as NSError).code == GIDSignInErrorCode.canceled.rawValue {
                executionObserver.complete(with: .none)
            } else {
                executionObserver.send(error: error)
            }
        } else if let user = user, let emailAddress = user.profile.email, let idToken = user.authentication?.idToken {
            completeLoginFlow(
                withEmailAddress: emailAddress,
                idToken: idToken,
                name: user.profile?.name ?? "",
                avatarURL: (user.profile?.hasImage ?? false) ? user.profile?.imageURL(withDimension: 640) : nil
            )
        } else {
            executionObserver.send(error: GoogleLoginError.invalidLoginDetails)
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        Logger.verbose()
    }
}

// MARK: - Errors
fileprivate enum GoogleLoginError: Error, LocalizedError {
    case invalidLoginDetails
    case corruptedAvatarImage
    
    var errorDescription: String? {
        switch self {
        case .invalidLoginDetails:
            return .txt("error_unknown")
        case .corruptedAvatarImage:
            return .txt("error_data_corruption")
        }
    }
}
