//
//  AppDelegate.swift
//  BeeLocation
//
//  Created by Wang Luo on 10/5/21.
//  Copyright © 2021 Wang Luo. All rights reserved.
//


import UIKit
import SeaLogger
import SeaTalkAuth
import ModuleBridge
import SeaLogger
import CommonUI
import SeaTalkTCP
import SeaTalkConfig
import SeaTalkDemo

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var bridge: SessionBridge?
    var authSession: AuthSession?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        SeaLogger.Logger.addConsoleLogger(level: .all)
        
        bridge = try! SessionModuleService.shared.loadModuleBridge(from: .main)
        let sessionStream = try! bridge!.resolve(ModuleSessionStream.identifier) as! ModuleSessionStream
        
        let window = UIWindow(frame: UIScreen.main.bounds)
        
        let authSession = AuthService.shared.buildAuthSession(window: window, bridge: bridge!, rootViewControllerBuilder: { (authInfo, bridge) in
            return ViewController(authInfo: authInfo, bridge: bridge)
        })
        self.authSession = authSession

        let googleHandler = GoogleLoginHandler(authSession: authSession)
        authSession.registerLoginHandler(googleHandler)
        
        if let authInfo = AuthService.shared.cachedAuthInfo() {
            window.rootViewController = ViewController(authInfo: authInfo, bridge: bridge!)
            sessionStream.authenticate()
        } else {
            window.rootViewController = authSession.buildViewController()
        }
        
        window.makeKeyAndVisible()
        self.window = window
        
        authSession.handleApplication(application, didFinishLaunchingWithOptions: launchOptions)

        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

}

