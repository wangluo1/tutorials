//
//  ViewController.swift
//  BeeLocation
//
//  Created by Wang Luo on 10/5/21.
//  Copyright © 2021 Wang Luo. All rights reserved.
//

import UIKit
import SeaTalkAuth
import ModuleBridge
import SeaLogger

class ViewController: UIViewController {
    private let authInfo: AuthInfo
    private let bridge: SessionBridge
    private let stack = UIStackView()

    init(authInfo: AuthInfo, bridge: SessionBridge) {
        self.authInfo = authInfo
        self.bridge = bridge
        super.init(nibName: nil, bundle: nil)
        
        bridge.willBeginSession(identifier: "\(authInfo.userID)")
        bridge.didBeginSession(identifier: "\(authInfo.userID)")
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        bridge.willEndSession(identifier: "\(authInfo.userID)")
        bridge.didEndSession(identifier: "\(authInfo.userID)")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        SeaLogger.Logger.info("Current UserID: \(authInfo.userID)")
        
        view.backgroundColor = .white

        stack.axis = .vertical
        stack.alignment = .center
        stack.distribution = .fillEqually
        view.addSubview(stack)

        let entryButton = Button()
        entryButton.setTitle("BeeLocation", for: .normal)
        entryButton.setTitleColor(.black, for: .normal)
        stack.addArrangedSubview(entryButton)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        stack.frame = view.bounds.insetBy(dx: 0, dy: 100)
    }
}

