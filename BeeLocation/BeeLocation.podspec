Pod::Spec.new do |s|
	s.name             = 'BeeLocation'
	s.version          = '0.0.1'
	s.summary          = 'A short description of BeeLocation.'

	s.homepage 		   = 'https://git.garena.com/ios-dev'
	s.license          = { :type => 'MIT', :file => 'LICENSE' }
	s.author           = { 'iOS' => 'Wang Luo' }
	s.source           = { :git => 'git@gitlab.com:wangluo1/tutorials.git', :tag => s.version.to_s }

	s.ios.deployment_target = '11.0'
	s.swift_version = '5.0'

	s.source_files = 'Sources/BeeLocation/Classes/**/*.swift'

    s.dependency 'BeeLocationProtocol'
    s.dependency 'SeaLogger'
end
