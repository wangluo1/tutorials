module Git
  extend self

  @@DEFAULT_REMOTE = "origin"
  
  def self.delete_branch(branch)
    execute("git branch -d #{branch}")
  end

  def self.checkout_branch(branch)
    execute("git checkout #{branch}")
  end

  def self.update_branch(branch, remote = @@DEFAULT_REMOTE)
    execute("git fetch -p")
    execute("git pull #{remote} #{branch}")
  end

  def self.tag(tag)
    execute("git tag #{tag}")
  end

  def self.push()
    execute("git push --tags")    
  end

  def self.current_branch()
    `git symbolic-ref --short HEAD`.strip
  end

  def self.check_changes(remote = @@DEFAULT_REMOTE)
    # See http://stackoverflow.com/a/2659808

    `git diff-index --quiet --cached HEAD`
    if $?.exitstatus != 0
      throw "You have uncommited changes!"
    end

    `git diff-files --quiet`
    if $?.exitstatus != 0
      throw "You have unstaged changes!"
    end

    output = `git ls-files --exclude-standard --others --error-unmatch`.strip
    if not output.empty?
      throw "You have untracked changes!"
    end

    curr_branch = current_branch()
    remote_head = `git rev-parse #{remote}/#{curr_branch}`.strip
    if $?.exitstatus != 0
      throw "You have not pushed your branch to remote!"
    end

    local_head = `git rev-parse #{curr_branch}`.strip
    if local_head != remote_head
      throw "You have not pushed your commits!"
    end
  end

  def self.execute(cmd)
    throw "Error executing git cmd! #{cmd}" unless system(cmd)
  end

  def self.execute_with_output(cmd)
    result = `#{cmd}`.strip
    if $?.exitstatus != 0
      throw "Failed to execute: #{cmd}"
    end
    result
  end
end