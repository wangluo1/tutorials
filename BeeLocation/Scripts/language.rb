require 'json'
require 'net/http'
require 'fileutils'

module Scripts

  class Language

    @@PROJECT_CODE    = "59c331881e61c20e7f8de04f"
    @@ENGLISH_URL     = "https://transify.seagroup.com/api/resources/#{@@PROJECT_CODE}/languagestoken/export/ios"
    @@TRANSLATION_URL = "https://transify.seagroup.com/api/resources/#{@@PROJECT_CODE}/languagestoken/%s/export/ios"

    @@PODNAME = "BeeLocation"

    @@FEATURE_TRANSLATION_ENABLED = false
    @@FEATURE_ENGLISH_URL         = "https://transify.seagroup.com/api/resources/%s/languagestoken/export/ios"
    @@FEATURE_TRANSLATION_URL     = "https://transify.seagroup.com/api/resources/%s/languagestoken/%s/export/ios"

    @@TMP_DIR = File.join(File.dirname(__FILE__), "language_temp")
    @@LANGUAGE_LIST_FILENAME = "language_list.json"
    @@LANGUAGE_CONFIG_FILENAME = "config_pod_language.json"

    @@LANGUAGES = [
        {
          "code" => "en",
          "name" => "English",
          "sequence" => 0,
          "server_code" => 0
        },
        {
          "code" => "zh-hans",
          "name" => "简体中文",
          "sequence" => 1,
          "server_code" => 4,
        },
        {
          "code" => "zh-hant",
          "name" => "繁體中文",
          "sequence" => 2,
          "server_code" => 5,
        },
        {
          "code" => "th",
          "name" => "ภาษาไทย",
          "sequence" => 3,
          "server_code" => 8,
        },
        {
          "code" => "vi",
          "name" => "Tiếng Việt",
          "sequence" => 4,
          "server_code" => 9
        },
        {
          "code" => "id",
          "name" => "Bahasa Indonesia",
          "sequence" => 5,
          "server_code" => 1

        }
    ]

    def self.update

      begin
        FileUtils.mkdir_p @@TMP_DIR
        download_langs(@@LANGUAGES, @@TMP_DIR, false, nil)
        build_language_list(@@LANGUAGES, @@TMP_DIR, false, nil)
        dest_dir = File.join(File.dirname(__FILE__), "../Pods/SeaTalkLang/SeaTalkLang/Resources/language_json")
        FileUtils.cp_r "#{@@TMP_DIR}/.", dest_dir
      ensure
        FileUtils.rm_r @@TMP_DIR
      end

      if @@FEATURE_TRANSLATION_ENABLED
        featureUpdate()
      end

      return dest_dir
    end

    private

    def self.featureUpdate  
      configFile = File.join(File.dirname(__FILE__), "../Sources/#{@@PODNAME}/Resources/Translations/language_json/#{@@LANGUAGE_CONFIG_FILENAME}")
      configFileInfo = JSON.parse(File.read(configFile))
      featureName = configFileInfo['featureName']
      id = configFileInfo['id']
      begin
        FileUtils.mkdir_p @@TMP_DIR
        download_langs(@@LANGUAGES, @@TMP_DIR, true, id)
        build_language_list(@@LANGUAGES, @@TMP_DIR, true, featureName)
        feature_dest_dir = File.join(File.dirname(__FILE__), "../Sources/#{@@PODNAME}/Resources/Translations/language_json")
        FileUtils.cp_r "#{@@TMP_DIR}/.", feature_dest_dir
      ensure
        FileUtils.rm_r @@TMP_DIR
      end
    end
    
    def self.download_langs (languages, output_dir, isFeatureSpecificLangs, resouceId)
      languages.each do |language|
        language_code = language["code"]
        url = language_url(language_code, isFeatureSpecificLangs, resouceId)

        request = Net::HTTP::Get.new(url)

        auth = "Token aa120a729708b1bc50bb3ee6959f06e22d693047"
        request['authorization'] = auth

        response = Net::HTTP.start(url.host, url.port, use_ssl: true) { |http|
          http.request(request)
        }

        File.write(File.join(output_dir, "#{language_code}.json"), response.body)

        puts "Downloaded #{language_code}: #{url}"
      end
    end

    def self.language_url(language_code, isFeatureSpecificLangs, resouceId)
      if isFeatureSpecificLangs
        if language_code == "en"
          URI.parse(@@FEATURE_ENGLISH_URL % resouceId) 
        else 
          URI.parse(@@FEATURE_TRANSLATION_URL % [resouceId, language_code])
        end
      else 
        if language_code == "en"
          URI.parse(@@ENGLISH_URL) 
        else 
          URI.parse(@@TRANSLATION_URL % language_code)
        end
      end
    end

    def self.build_language_list (languages, output_dir, isFeatureSpecificLangs, featureName)

      language_list = {
        "last_updated" => Time.now.to_i,
        "languages"    => languages
      }

      json = JSON.pretty_generate(language_list)
      File.write(File.join(output_dir, @@LANGUAGE_LIST_FILENAME), json)

      if isFeatureSpecificLangs
        puts "Updated #{featureName} specific language list: #{json}"
      else 
        puts "Updated language list: #{json}"
      end
    end
  end
end
