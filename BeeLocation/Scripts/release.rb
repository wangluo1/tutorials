require_relative 'libs/git.rb'
require 'cocoapods-core'

module Scripts
  class Release
    def self.bump(version, name)
      Git.check_changes()

      protocol_path = path_from_name(name + "Protocol")
      module_path = path_from_name(name)

      version_param = ""
      if version.nil? || version.empty? 
        version_param = "bump_type:\"patch\""
      else
        version_param = "version_number:\"#{version}\""
      end

      puts "Bumping version for protocol..."
      execute("fastlane run version_bump_podspec path:\"#{protocol_path}\" #{version_param}")
      puts ""
      
      puts "Bumping version for module..."
      execute("fastlane run version_bump_podspec path:\"#{module_path}\" #{version_param}")
      puts ""
      
      spec = Pod::Specification.from_file(protocol_path)
      commit_and_tag(spec.version, protocol_path, module_path)
    end

    def self.commit_and_tag(version, protocol_path, module_path)
      puts "Committing..."
      execute("git add \"#{protocol_path}\"")
      execute("git add \"#{module_path}\"")
      execute("git commit -m \"Bumping version to #{version}\"")

      execute("git push")
      puts ""
      puts "Tagging..."
      execute("git tag #{version}")
      execute("git push --tags")
      puts ""
    end

    def self.pod_push(name)
      puts "Releasing for protocol..."
      execute("pod repo push #{get_repo_name} #{name}Protocol.podspec --allow-warnings")
      puts ""

      puts "Releasing for module..."
      execute("pod repo push #{get_repo_name} #{name}.podspec --allow-warnings")
      puts ""
    end

    def self.release(version, name)
      bump(version, name)
      pod_push(name)
    end

    private

    def self.execute(cmd)
      Git.execute(cmd)
    end

    def self.path_from_name(name)
      return File.join(File.dirname(__FILE__), "../#{name}.podspec")
    end

    def self.get_repo_name()
      token = ENV["SEATALK_POD_REPO_NAME"]
      
      if token.nil?
        throw "Missing SEATALK_POD_REPO_NAME in environment variables."
      end
      token
    end
  end
end
