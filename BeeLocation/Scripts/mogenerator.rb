require 'fileutils'

module Scripts

  class Mogenerator
    @@TEMPLATE_DIR_PATH = File.join(File.dirname(__FILE__), "./mo_templates/")

    def self.generate(pod_name)
      source_dir = File.join(File.dirname(__FILE__), "../Sources/#{pod_name}/Resources/CoreData/")
      target_dir = File.join(File.dirname(__FILE__), "../Sources/#{pod_name}/Classes/CoreData/")
      generate_in_dir(source_dir, target_dir)
    end

    private 

    def self.generate_in_dir(source_dir, target_dir)
      machineDirPath = File.join(target_dir, "MachineModels")
      humanDirPath = File.join(target_dir, "Models")

      FileUtils.mkdir_p machineDirPath
      FileUtils.mkdir_p humanDirPath

      filePaths = Dir.glob(File.join(source_dir, "*.xcdatamodeld"))
      filePath = filePaths.sort { |path| File.mtime(path) }.first

      puts ""
      puts "Generating: #{File.basename(filePath)}"
      puts "Path: #{filePath}"

      cmd = "mogenerator"
      cmd << " --swift"
      cmd << " --model #{filePath}"
      cmd << " --machine-dir #{machineDirPath}"
      cmd << " --human-dir #{humanDirPath}"
      cmd << " --template-path #{@@TEMPLATE_DIR_PATH}"

      system(cmd)
    end
  end
end